using UnityEngine;
public class Death : MonoBehaviour

{
    public Death movement;

    void OnCollisionEnter(Collision collisionInfo)
    {
        if (collisionInfo.collider.tag == "Obstacle")
        {
            Destroy(gameObject);
            Debug.Log("dead");
        }
    }
}
